import requests
import pprint
from selenium import webdriver

def get_top_google():
	top = []
	browser = webdriver.Chrome()
	browser.get("https://trends.google.com/trends/trendingsearches/daily?geo=US")
	today_list = browser.find_element_by_class_name("feed-list-wrapper")
	search_list = today_list.find_elements_by_class_name("feed-item")
	# print(len(search_list))
	for item in search_list:
		titleClass = item.find_element_by_class_name("title")
		# print(titleClass.text)
		top.append(titleClass.text)
	return top

def get_top_twitter():
	top = []
	browser = webdriver.Chrome()
	browser.get("http://tweeplers.com/hashtags/?cc=US")
	panel = browser.find_element_by_class_name("panel-body")
	id_number = 1
	for i in range(0, 20):
		s_id = str(id_number)
		id_string = "row_" + s_id
		row = panel.find_element_by_id(id_string)
		row_id = "item_u_" + s_id
		row_hashtag = row.find_element_by_id(row_id)
		# print(row_hashtag.text)
		top.append(row_hashtag.text)
		id_number += 1
	return top

if __name__ == "__main__":
	google_top = get_top_google()
	hashtag_top = get_top_twitter()
	top_dict = {"Google Searches" : google_top, "Twitter Hashtags" : hashtag_top}
	pprint.pprint(top_dict)
	with open("results.txt", 'w') as dump:
		pprint.pprint(top_dict, stream=dump)
